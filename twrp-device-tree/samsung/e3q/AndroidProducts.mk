#
# Copyright (C) 2024 The Android Open Source Project
# Copyright (C) 2024 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_e3q.mk

COMMON_LUNCH_CHOICES := \
    omni_e3q-user \
    omni_e3q-userdebug \
    omni_e3q-eng
