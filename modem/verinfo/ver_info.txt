{
    "Image_Build_IDs": {
        "adsp": "LPAIDSP.HT.1.0-00657.1-LANAI-1",
        "aop": "AOP.HO.5.0-00574-LANAI_E-1.47614.14",
        "apps_LE": "LE.UM.7.3.1.r1-10300-genericarmv8-64.0-1",
        "apps_kernel": "KERNEL.PLATFORM.3.0.r1-04500-kernel.0-1",
        "apps_qssi": "LA.QSSI.14.0.r1-09700-qssi.0-1",
        "apps_vendor": "LA.VENDOR.14.3.0.r1-06300-lanai.0-1",
        "audio": "AUDIO.LA.9.0.r1-03000-lanai.0-1",
        "boot": "BOOT.MXF.2.1-01661-LANAI-1.47555.8",
        "btfm_hmt": "BTFW.HAMILTON.2.0.1-00265-PATCHZ-1",
        "camera": "CAMERA.LA.4.0.r2-02600-lanai.0-1",
        "cdsp": "CDSP.HT.3.0-00680.6-LANAI-1.51328.4.52078.9",
        "common": "Lanai.LA.1.0.r1-00740-STD.PROD-1",
        "cpucp": "CPUCP.FW.1.0-00072-LANAI.EXT-1",
        "cpuss_vm": "CPUSS.CPUSYS.VM.1.0-00012-LANAI.EXT-1",
        "cv": "CV.LA.2.0.r1-02600-lanai.0-1",
        "display": "DISPLAY.LA.4.0.r2-03500-lanai.0-1",
        "glue": "GLUE.LANAI.LA.1.0-00225-NOOP_TEST_LANAI-1",
        "graphics": "GRAPHICS.LA.14.0.r1-03200-lanai.0-1",
        "hexlp": "HEXLP.LA.1.0.r1-00600-lanai.0-1",
        "modem": "MPSS.DE.5.0-02194-LANAI_GEN_PACK-1",
        "sensors": "SENSORS.LA.4.0.r2-01500-lanai.0-1",
        "spss": "SPSS.A1.1.9-00057-LANAI-1",
        "tz": "TZ.XF.5.0-07584-LANAIAAAAANAZT-1.47632.22",
        "tz_apps": "TZ.APPS.1.0-01497-LANAIAAAAANAZT-2",
        "video": "VIDEO.LA.4.0.r2-02400-lanai.0-1",
        "xr": "XR.LA.1.0.r3-02400-lanai.0-1"
    },
    "Metabuild_Info": {
        "Meta_Build_ID": "Lanai.LA.1.0.r1-00740-STD.PROD-1",
        "Product_Flavor": "asic",
        "Time_Stamp": "2024-04-30 19:25:42"
    },
    "Version": "1.0"
}